# Sieve rules

## What is this?

This repository contains rules to tag (add headers to) specific email messages, to allow for easier matching down the line.

## Installation

Simply run `./generate-full.py`, and copy the output to your clipboard. Then paste it into your email service of choice.

To prevent spoofing of added tags, add a line like this to your Sieve config **before** the generated output:

```sieve
# Remove "internal" headers from external email
deleteheader "X-Sieve-match";
```

## List of tags

- `security.{company}`: Security emails
- `ephemeral.{company}`: One-off emails to convey a code / sign-in link
- `receipt.{company}`: Purchase confirmations and receipts.

Please note, these tags may overlap.
