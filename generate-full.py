#!/usr/bin/env python3

import os

for parent, _, files in os.walk("matchers"):
    for file in files:
        if not file.endswith(".sieve"):
            continue

        full_path = os.path.join(parent, file)
        print(f"# {full_path}")
        with open(full_path, "r") as f:
            print(f.read().strip())
