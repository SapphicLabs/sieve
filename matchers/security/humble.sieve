if allof(
  address :is "From" "noreply@humblebundle.com",
  header :is "Subject" "Humble Bundle account protection"
) {
  addheader "X-Sieve-match" "security.humble";
  addheader "X-Sieve-match" "ephemeral.humble";
}
