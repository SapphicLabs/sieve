if anyof(
  address :is "From" [
    "appleid@id.apple.com",
    "noreply@apple.com"
  ],
  allof(
    address :is "From" "noreply@email.apple.com",
    header :matches "Subject" [
      "Your Apple ID was used to sign in to iCloud*",
      "Find My has been disabled on*"
    ]
  )
) {
  addheader "X-Sieve-match" "security.apple";
}
