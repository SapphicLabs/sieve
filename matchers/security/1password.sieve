if allof(
  address :is "From" "hello@1password.com",
  header :matches "Subject" "New 1Password sign-in from*"
) {
  addheader "X-Sieve-match" "security.1password";
}
