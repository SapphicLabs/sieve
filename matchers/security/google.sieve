if allof(
  address :is "From" "no-reply@accounts.google.com",
  header :matches "Subject" "Security alert*"
) {
  addheader "X-Sieve-match" "security.google";
}
