if allof(
  address :is "From" "noreply@steampowered.com",
  header :matches "Subject" "Your Steam account: Access from new*"
) {
  addheader "X-Sieve-match" "security.steam";
}
