if allof(
  address :is "From" "account-security-noreply@accountprotection.microsoft.com",
  header :is "Subject" [
    "New sign-in detected for your Microsoft account",
    "Microsoft account security code",
    "Microsoft account security info was added",
    "Microsoft account security info was deleted",
    "New app(s) connected to your Microsoft account"
  ]
) {
  addheader "X-Sieve-match" "security.microsoft";
  if header :is "Subject" "Microsoft account security code" {
    addheader "X-Sieve-match" "ephemeral.microsoft";
  }
}
