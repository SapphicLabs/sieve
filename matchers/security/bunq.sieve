if anyof (
  allof (
    address :is "From" "noreply@bunq.com",
    header :is "Subject" "You logged in to bunq on a new device"
  ),
  allof (
    address :is "From" "no-reply@hello.bunq.com",
    header :is "Subject" [
      "📬You sent an invite to share an account",
      "🙌Your Connect invite was accepted!"
    ]
  )
) {
  addheader "X-Sieve-match" "security.bunq";
}
