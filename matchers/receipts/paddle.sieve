if allof(
  address :is "From" "help@paddle.com",
  header :matches "Subject" ["*order", "*receipt"]
) {
  addheader "X-Sieve-match" "receipt.paddle";
}
