if allof(
  address :is "From" "noreply@bandcamp.com",
  header :is "Subject" "Thank you!"
) {
  addheader "X-Sieve-match" "receipt.bandcamp";
}
