if allof(
  address :is "From" "support@fastmail.com",
  header :is "Subject" "Fastmail payment receipt"
) {
  addheader "X-Sieve-match" "receipt.fastmail";
}
