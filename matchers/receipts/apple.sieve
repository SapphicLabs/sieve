if allof(
  address :is "From" "no_reply@email.apple.com",
  header :matches "Subject" ["*purchase*", "*subscription*"]
) {
  addheader "X-Sieve-match" "receipt.apple";
}
